Taxonomy Term Preview
=====================

This module enables previewing taxonomy terms like it's done for nodes.

**Warning** This replaces the default form class for the taxonomy terms and is
therefore not compatible with other modules that do the same.

Requirements
------------

This module requires no modules outside the Drupal core.

Installation
------------

Install the Cloner module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.

Alternatives
------------

- [All Entity Preview](https://www.drupal.org/project/all_entity_preview)

TODO
----

- [ ] Add tests.
